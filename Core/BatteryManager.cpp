#include "stdafx.h"
#include "BatteryManager.h"
#include "../Utilities/VirtualFile.h"
#include "../Utilities/FolderUtilities.h"

void BatteryManager::Initialize(string romName)
{
	_romName = romName;
}

string BatteryManager::GetBasePath()
{
	return FolderUtilities::CombinePath(FolderUtilities::GetSaveFolder(), _romName);
}

void BatteryManager::SetBatteryProvider(shared_ptr<IBatteryProvider> provider)
{
	_provider = provider;
}

void BatteryManager::SetBatteryRecorder(shared_ptr<IBatteryRecorder> recorder)
{
	_recorder = recorder;
}

void BatteryManager::SaveBattery(string extension, uint8_t* data, uint32_t length)
{
	ofstream out(GetBasePath() + extension, ios::binary);
	if(out) {
		out.write((char*)data, length);
	}
}

vector<uint8_t> BatteryManager::LoadBattery(string extension)
{
	vector<uint8_t> batteryData;
	VirtualFile file = GetBasePath() + extension;
	if(file.IsValid()) {
		file.ReadFile(batteryData);
	}

	return batteryData;
}

void BatteryManager::LoadBattery(string extension, uint8_t* data, uint32_t length)
{
	vector<uint8_t> batteryData = LoadBattery(extension);
	memcpy(data, batteryData.data(), std::min((uint32_t)batteryData.size(), length));
}
