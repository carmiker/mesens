SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

CC ?= cc
CXX ?= c++
CFLAGS ?= -O2
CXXFLAGS ?= -O2
FLAGS_SZ += -fPIC -pthread
FLAGS := -std=c++17 -pthread
INCLUDES := -I$(SOURCEDIR)
WARNINGS := -Wall

CORE_DIR := Core
UTIL_DIR := Utilities

PKG_CONFIG ?= pkg-config
CFLAGS_JG := $(shell $(PKG_CONFIG) --cflags jg)

LIBS := -lm -lpthread -lstdc++
PIC := -fPIC
SHARED := $(PIC)

NAME := mesens
PREFIX ?= /usr/local
LIBDIR ?= $(PREFIX)/lib
DATAROOTDIR ?= $(PREFIX)/share
DOCDIR ?= $(DATAROOTDIR)/doc/$(NAME)

UNAME := $(shell uname -s)
ifeq ($(UNAME), Darwin)
	FLAGS += -stdlib=libc++
	SHARED += -dynamiclib
	TARGET := $(NAME).dylib
else ifeq ($(OS), Windows_NT)
	SHARED += -shared
	TARGET := $(NAME).dll
else
	SHARED += -shared
	TARGET := $(NAME).so
endif

CXXSRCS := $(CORE_DIR)/AluMulDiv.cpp \
	$(CORE_DIR)/Assembler.cpp \
	$(CORE_DIR)/BaseCartridge.cpp \
	$(CORE_DIR)/BaseControlDevice.cpp \
	$(CORE_DIR)/BaseRenderer.cpp \
	$(CORE_DIR)/BaseSoundManager.cpp \
	$(CORE_DIR)/BaseVideoFilter.cpp \
	$(CORE_DIR)/BatteryManager.cpp \
	$(CORE_DIR)/Breakpoint.cpp \
	$(CORE_DIR)/BreakpointManager.cpp \
	$(CORE_DIR)/BsxCart.cpp \
	$(CORE_DIR)/BsxMemoryPack.cpp \
	$(CORE_DIR)/BsxSatellaview.cpp \
	$(CORE_DIR)/BsxStream.cpp \
	$(CORE_DIR)/CallstackManager.cpp \
	$(CORE_DIR)/CheatManager.cpp \
	$(CORE_DIR)/CodeDataLogger.cpp \
	$(CORE_DIR)/Console.cpp \
	$(CORE_DIR)/ConsoleLock.cpp \
	$(CORE_DIR)/ControlManager.cpp \
	$(CORE_DIR)/Cpu.cpp \
	$(CORE_DIR)/CpuDebugger.cpp \
	$(CORE_DIR)/CpuDisUtils.cpp \
	$(CORE_DIR)/Cx4.cpp \
	$(CORE_DIR)/Cx4.Instructions.cpp \
	$(CORE_DIR)/Cx4DisUtils.cpp \
	$(CORE_DIR)/Cx4Debugger.cpp \
	$(CORE_DIR)/Debugger.cpp \
	$(CORE_DIR)/DebugHud.cpp \
	$(CORE_DIR)/DebugStats.cpp \
	$(CORE_DIR)/DefaultVideoFilter.cpp \
	$(CORE_DIR)/Disassembler.cpp \
	$(CORE_DIR)/DisassemblyInfo.cpp \
	$(CORE_DIR)/DmaController.cpp \
	$(CORE_DIR)/EmuSettings.cpp \
	$(CORE_DIR)/EventManager.cpp \
	$(CORE_DIR)/ExpressionEvaluator.cpp \
	$(CORE_DIR)/Gameboy.cpp \
	$(CORE_DIR)/GbAssembler.cpp \
	$(CORE_DIR)/GbCpu.cpp \
	$(CORE_DIR)/GbDmaController.cpp \
	$(CORE_DIR)/GbPpu.cpp \
	$(CORE_DIR)/GbApu.cpp \
	$(CORE_DIR)/GbNoiseChannel.cpp \
	$(CORE_DIR)/GbSquareChannel.cpp \
	$(CORE_DIR)/GbWaveChannel.cpp \
	$(CORE_DIR)/GbTimer.cpp \
	$(CORE_DIR)/GbMemoryManager.cpp \
	$(CORE_DIR)/GbDebugger.cpp \
	$(CORE_DIR)/GbEventManager.cpp \
	$(CORE_DIR)/GameboyDisUtils.cpp \
	$(CORE_DIR)/Gsu.cpp \
	$(CORE_DIR)/Gsu.Instructions.cpp \
	$(CORE_DIR)/GsuDisUtils.cpp \
	$(CORE_DIR)/GsuDebugger.cpp \
	$(CORE_DIR)/InputHud.cpp \
	$(CORE_DIR)/InternalRegisters.cpp \
	$(CORE_DIR)/KeyManager.cpp \
	$(CORE_DIR)/LabelManager.cpp \
	$(CORE_DIR)/MemoryAccessCounter.cpp \
	$(CORE_DIR)/MemoryDumper.cpp \
	$(CORE_DIR)/MemoryManager.cpp \
	$(CORE_DIR)/MemoryMappings.cpp \
	$(CORE_DIR)/MessageManager.cpp \
	$(CORE_DIR)/Msu1.cpp \
	$(CORE_DIR)/Multitap.cpp \
	$(CORE_DIR)/NecDsp.cpp \
	$(CORE_DIR)/NecDspDebugger.cpp \
	$(CORE_DIR)/NecDspDisUtils.cpp \
	$(CORE_DIR)/NotificationManager.cpp \
	$(CORE_DIR)/NtscFilter.cpp \
	$(CORE_DIR)/Obc1.cpp \
	$(CORE_DIR)/PcmReader.cpp \
	$(CORE_DIR)/Ppu.cpp \
	$(CORE_DIR)/PpuTools.cpp \
	$(CORE_DIR)/Profiler.cpp \
	$(CORE_DIR)/RegisterHandlerB.cpp \
	$(CORE_DIR)/RewindData.cpp \
	$(CORE_DIR)/RewindManager.cpp \
	$(CORE_DIR)/Rtc4513.cpp \
	$(CORE_DIR)/SaveStateManager.cpp \
	$(CORE_DIR)/Sa1.cpp \
	$(CORE_DIR)/Sa1Cpu.cpp \
	$(CORE_DIR)/ScaleFilter.cpp \
	$(CORE_DIR)/ScriptHost.cpp \
	$(CORE_DIR)/ScriptingContext.cpp \
	$(CORE_DIR)/ScriptManager.cpp \
	$(CORE_DIR)/Sdd1Decomp.cpp \
	$(CORE_DIR)/Sdd1.cpp \
	$(CORE_DIR)/Sdd1Mmc.cpp \
	$(CORE_DIR)/ShortcutKeyHandler.cpp \
	$(CORE_DIR)/SnesController.cpp \
	$(CORE_DIR)/SoundMixer.cpp \
	$(CORE_DIR)/SoundResampler.cpp \
	$(CORE_DIR)/Spc.cpp \
	$(CORE_DIR)/Spc.Instructions.cpp \
	$(CORE_DIR)/SpcDebugger.cpp \
	$(CORE_DIR)/SpcDisUtils.cpp \
	$(CORE_DIR)/SpcHud.cpp \
	$(CORE_DIR)/SPC_DSP.cpp \
	$(CORE_DIR)/SPC_Filter.cpp \
	$(CORE_DIR)/Spc7110.cpp \
	$(CORE_DIR)/Spc7110Decomp.cpp \
	$(CORE_DIR)/SuperGameboy.cpp \
	$(CORE_DIR)/stdafx.cpp \
	$(CORE_DIR)/TraceLogger.cpp \
	$(CORE_DIR)/VideoDecoder.cpp \
	$(CORE_DIR)/VideoRenderer.cpp \
	$(CORE_DIR)/WaveRecorder.cpp \
	$(UTIL_DIR)/AutoResetEvent.cpp \
	$(UTIL_DIR)/blip_buf.cpp \
	$(UTIL_DIR)/BpsPatcher.cpp \
	$(UTIL_DIR)/CRC32.cpp \
	$(UTIL_DIR)/Equalizer.cpp \
	$(UTIL_DIR)/FolderUtilities.cpp \
	$(UTIL_DIR)/GifRecorder.cpp \
	$(UTIL_DIR)/HermiteResampler.cpp \
	$(UTIL_DIR)/HexUtilities.cpp \
	$(UTIL_DIR)/IpsPatcher.cpp \
	$(UTIL_DIR)/md5.cpp \
	$(UTIL_DIR)/miniz.cpp \
	$(UTIL_DIR)/PlatformUtilities.cpp \
	$(UTIL_DIR)/Serializer.cpp \
	$(UTIL_DIR)/sha1.cpp \
	$(UTIL_DIR)/SimpleLock.cpp \
	$(UTIL_DIR)/snes_ntsc.cpp \
	$(UTIL_DIR)/stb_vorbis.cpp \
	$(UTIL_DIR)/stdafx.cpp \
	$(UTIL_DIR)/Timer.cpp \
	$(UTIL_DIR)/UpsPatcher.cpp \
	$(UTIL_DIR)/UTF8Util.cpp \
	$(UTIL_DIR)/VirtualFile.cpp \
	$(UTIL_DIR)/HQX/hq2x.cpp \
	$(UTIL_DIR)/HQX/hq3x.cpp \
	$(UTIL_DIR)/HQX/hq4x.cpp \
	$(UTIL_DIR)/HQX/init.cpp \
	$(UTIL_DIR)/KreedSaiEagle/2xSai.cpp \
	$(UTIL_DIR)/KreedSaiEagle/Super2xSai.cpp \
	$(UTIL_DIR)/KreedSaiEagle/SuperEagle.cpp \
	$(UTIL_DIR)/Scale2x/scale2x.cpp \
	$(UTIL_DIR)/Scale2x/scale3x.cpp \
	$(UTIL_DIR)/Scale2x/scalebit.cpp \
	$(UTIL_DIR)/xBRZ/xbrz.cpp \
	jg.cpp

# Object dirs
MKDIRS := $(CORE_DIR) \
	$(UTIL_DIR)/KreedSaiEagle \
	$(UTIL_DIR)/HQX \
	$(UTIL_DIR)/Scale2x \
	$(UTIL_DIR)/xBRZ

OBJDIR := objs

# List of object files
OBJS := $(patsubst %,$(OBJDIR)/%,$(CSRCS:.c=.o) $(CXXSRCS:.cpp=.o))

# Compiler command
COMPILE = $(strip $(1) $(CPPFLAGS) $(PIC) $(2) -c $< -o $@)
COMPILE_CXX = $(call COMPILE, $(CXX) $(CXXFLAGS), $(1))

# Info command
COMPILE_INFO = $(info $(subst $(SOURCEDIR)/,,$(1)))

# Core commands
BUILD_JG = $(call COMPILE_CXX, $(FLAGS) $(WARNINGS) $(CFLAGS_JG))
BUILD_MAIN = $(call COMPILE_CXX, $(FLAGS) $(WARNINGS))

.PHONY: all clean install install-strip uninstall

all: $(NAME)/$(TARGET)

# Shim Rules
$(OBJDIR)/jg.o: $(SOURCEDIR)/jg.cpp $(OBJDIR)/.tag
	$(call COMPILE_INFO, $(BUILD_JG))
	@$(BUILD_JG)

# Core Rules
$(OBJDIR)/%.o: $(SOURCEDIR)/%.cpp $(OBJDIR)/.tag
	$(call COMPILE_INFO, $(BUILD_MAIN))
	@$(BUILD_MAIN)

$(OBJDIR)/.tag:
	@mkdir -p -- $(patsubst %,$(OBJDIR)/%,$(MKDIRS))
	@touch $@

$(NAME)/$(TARGET): $(OBJS)
	@mkdir -p $(NAME)
	$(CXX) $(OBJS) $(SHARED) $(LDFLAGS) $(LIBS) -o $@

clean:
	rm -rf $(OBJDIR)/ $(NAME)/

install: all
	@mkdir -p $(DESTDIR)$(DOCDIR)
	@mkdir -p $(DESTDIR)$(LIBDIR)/jollygood
	cp $(NAME)/$(TARGET) $(DESTDIR)$(LIBDIR)/jollygood/
	cp $(SOURCEDIR)/README $(DESTDIR)$(DOCDIR)
	cp $(SOURCEDIR)/LICENSE $(DESTDIR)$(DOCDIR)

install-strip: install
	strip $(DESTDIR)$(LIBDIR)/jollygood/$(TARGET)

uninstall:
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -f $(DESTDIR)$(LIBDIR)/jollygood/$(TARGET)
